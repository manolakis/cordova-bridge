(function(window){
	'use strict';

	var app = window.app;

	window.device = {
		model: 'PhantomJS',
		cordova: '0.0.0',
		platform: 'Testing',
		uuid: '0123456789',
		version: '0.0'
	};

	describe('device', function(){
		var mock = {
			test: function(){}
		};

		beforeEach(function(){
			spyOn(mock, 'test');
			spyOn(app, 'response').andCallFake(function(data){
				mock.test({
					uuid: this.uuid,
					data: data
				});
			});
		});

		it('should obtain device', function(){
			var uuid;

			runs(function() {
				uuid = helper.postMessage('device');
			});

			waitsFor(function() {
				return (mock.test.calls.length > 0);
			}, 'response should be called once', 500);

			runs(function() {
				expect(mock.test).toHaveBeenCalledWith({ 
					uuid: uuid,
					data: {
						device: window.device
					}
				});
			});
		});

	});
	
})(this);