(function(window){
	'use strict';

	var app = window.app,
		camera = navigator.camera = {
			getPicture: function(success, error, options){},
			cleanup: function(success, error){}
		};

	describe('camera', function(){
		var mock = {
			test: function(success, error){}
		};

		beforeEach(function(){
			spyOn(mock, 'test');
			spyOn(app, 'response').andCallFake(function(data){
				mock.test({
					uuid: this.uuid,
					data: data
				});
			});
		});

		it('should call success if it can remove intermediate photos', function(){
			var uuid;

			spyOn(camera, 'cleanup').andCallFake(function(success, error){
				success();
			});

			runs(function() {
				uuid = helper.postMessage('camera.cleanup');
			});

			waitsFor(function() {
				return (mock.test.calls.length > 0);
			}, 'response should be called once', 500);

			runs(function() {
				expect(mock.test).toHaveBeenCalledWith({ 
					uuid: uuid,
					data: {
						success: true
					}
				});
			});
		});

		it('should call error if it can\'t remove intermediate photos', function(){
			var uuid,
				message = 'error in camera';

			spyOn(camera, 'cleanup').andCallFake(function(success, error){
				error(message);
			});

			runs(function() {
				uuid = helper.postMessage('camera.cleanup');
			});

			waitsFor(function() {
				return (mock.test.calls.length > 0);
			}, 'response should be called once', 500);

			runs(function() {
				expect(mock.test).toHaveBeenCalledWith({ 
					uuid: uuid,
					data: {
						error: message
					}
				});
			});
		});

		it('should call success if it can take a picture', function(){
			var uuid;

			spyOn(camera, 'getPicture').andCallFake(function(success, error, options){
				success('imageData');
			});

			runs(function() {
				uuid = helper.postMessage('camera.getPicture');
			});

			waitsFor(function() {
				return (mock.test.calls.length > 0);
			}, 'response should be called once', 500);

			runs(function() {
				expect(mock.test).toHaveBeenCalledWith({ 
					uuid: uuid,
					data: {
						success: 'imageData'
					}
				});
			});
		});

		it('should call error if it can\'t take a picture', function(){
			var uuid,
				message = 'error in camera';

			spyOn(camera, 'getPicture').andCallFake(function(success, error){
				error(message);
			});

			runs(function() {
				uuid = helper.postMessage('camera.getPicture');
			});

			waitsFor(function() {
				return (mock.test.calls.length > 0);
			}, 'response should be called once', 500);

			runs(function() {
				expect(mock.test).toHaveBeenCalledWith({ 
					uuid: uuid,
					data: {
						error: message
					}
				});
			});
		});
		
	});
	
})(this);