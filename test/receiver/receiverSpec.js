/* global alert, console, xdescribe */
(function(window) {
    'use strict';

    var document = window.document,
        helper = window.helper,
        app = window.app;

    describe('app', function() {
        describe('initialize', function() {
            it('should bind deviceready', function() {
                var foo = {
                    receivedEvent : function(event){}
                };

                document.addEventListener('applicationready', function(){
                    foo.receivedEvent('applicationready');
                }, false);

                runs(function() {
                    spyOn(foo, 'receivedEvent').andCallThrough();
                    app.initialize(helper.domain, '/index.html');
                    helper.trigger(document, 'deviceready');
                });

                waitsFor(function() {
                    return (foo.receivedEvent.calls.length > 0);
                }, 'receivedEvent should be called once', 500);

                runs(function() {
                    expect(foo.receivedEvent).toHaveBeenCalled();
                });
            });
        });

        describe('addPlugin', function(){
            it('should add a plugin', function() {
                var uuid;

                runs(function() {
                    spyOn(app, 'response');
                    app.addPlugin({
                        'test': function() {
                            uuid = app.response('test');
                        }
                    });
                    helper.postMessage('test');
                });

                waitsFor(function() {
                    return (app.response.calls.length > 0);
                }, 'response should be called once', 500);

                runs(function() {
                    expect(app.response).toHaveBeenCalledWith('test');
                });
            });
        });
    });
})(this);