/* global console */
(function(window){
	'use strict';

	var s4 = function() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	};

	window.helper = {
		domain: 'http://localhost:9876',
		trigger: function(obj, name) {
			var e = window.document.createEvent('Event');
			e.initEvent(name, false, false);
			obj.dispatchEvent(e);
		},
		postMessage: function() {
			var args = Array.prototype.slice.call(arguments),
				uuid = this.uuid(),
				data = {
					uuid: uuid,
					signature: args.shift(),
					params: args
				};

			window.postMessage(data, this.domain);

			return uuid;
		},
		uuid: function() {
			return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
		}
	};
})(this);