module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      sender: {
        src: ['lib/<%= pkg.name %>-sender.js', 'lib/sender-*-plugin.js'],
        dest: 'dist/<%= pkg.name %>-sender.js'
      },
      receiver: {
        src: ['lib/<%= pkg.name %>-receiver.js', 'lib/receiver-*-plugin.js'],
        dest: 'dist/<%= pkg.name %>-receiver.js'
      },
    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      sender: {
        src: '<%= concat.sender.dest %>',
        dest: 'dist/<%= pkg.name %>-sender.min.js'
      },
      receiver: {
        src: '<%= concat.receiver.dest %>',
        dest: 'dist/<%= pkg.name %>-receiver.min.js'
      },
    },
    copy: {
      build: {
        files: [
          {expand: true, cwd: 'dist', src: ['cordova-bridge-receiver.min.js'], dest: '../ejemplo/www/js'}
        ]
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib: {
        options: {
          jshintrc: 'lib/.jshintrc'
        },
        src: ['lib/**/*.js']
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/**/*.js']
      },
    },
    karma: {
      receiver: {
        options: {
          frameworks: ['jasmine'],
          browsers: ['PhantomJS'],
          singleRun: true,
          files: [
            'lib/cordova-bridge-receiver.js',
            'lib/receiver-*-plugin.js',
            'test/receiver/helper.js',
            'test/receiver/receiverSpec.js',
            'test/receiver/*PluginSpec.js'
          ]
        }
      },/*
      sender: {
        options: {
          frameworks: ['jasmine'],
          browsers: ['PhantomJS'],
          singleRun: true,
          files: [
            'lib/*-sender.js',
            'test/sender/*.js'
          ]
        }
      }*/
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib: {
        files: '<%= jshint.lib.src %>',
        tasks: ['jshint:lib', 'default']
      },
      test: {
        files: '<%= jshint.test.src %>',
        tasks: ['jshint:test', 'default']
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-karma');

  grunt.registerTask('develop', ['jshint', 'karma']);

  grunt.registerTask('default', ['develop', 'concat', 'uglify', 'copy']);
};
