/* global alert */
(function(window){
	'use strict';

	var s4 = function() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		},
		
		helper = {
			domain: 'http://localhost:3000',
			uuid : function() {
				return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
			},
			trigger: function(obj, name) {
				var e = window.document.createEvent('Event');
				e.initEvent(name, false, false);
				obj.dispatchEvent(e);
			},
			sendMessage: function(data) {
				window.parent.postMessage(data, 'file://');
			},
			prepareMessage: function(signature) {
				return {
					uuid: helper.uuid(),
					signature: signature,
					params: {}
				};
			}
		},
		cache = {};

	if (window.self !== window.top) {
		window.cordova = {
			cache: cache,
			sendMessage: helper.sendMessage,
			prepareMessage: helper.prepareMessage
		};

		window.addEventListener('message', function(event){
			if (event.origin === 'file://') {
				var data = event.data;
				if (data.uuid in cache) {
					cache[data.uuid](data);
				}
			}
		}, false);
	}
	
})(this);