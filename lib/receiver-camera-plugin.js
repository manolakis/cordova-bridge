(function(window){

	window.app.addPlugin({
		'camera.getPicture': function(){
			var uuid = this.uuid,
				options = this.params.options || {};

			navigator.camera.getPicture(function(imageData){
				window.app.response.apply({ uuid: uuid }, [{ success: imageData }]);
			}, function(message) {
				window.app.response.apply({ uuid: uuid }, [{ error: message }]);
			}, options);
		},

		'camera.cleanup': function(){
			var uuid = this.uuid;

			navigator.camera.cleanup(function(){
				window.app.response.apply({ uuid: uuid }, [{ success: true }]);
			}, function(message){
				window.app.response.apply({ uuid: uuid }, [{ error: message }]);
			});
		}
	});

})(this);