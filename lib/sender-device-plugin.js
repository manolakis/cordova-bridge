(function(window, cordova){
	'use strict';

	if (cordova) {
		var getDeviceModel = function(){				
				var message = cordova.prepareMessage('device');

				cordova.cache[message.uuid] = function(message) {
					window.device = message.data.device;

					delete cordova.cache[message.uuid];
				};

				cordova.sendMessage(message);
			};
		
		getDeviceModel();
		
	}
})(window, window.cordova);