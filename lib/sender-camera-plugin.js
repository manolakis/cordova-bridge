(function(window, cordova){
	'use strict';

	var isUndefined = function(value) {
			return typeof value === 'undefined';
		},

		getPicture = function(success, error, options){
			var message = cordova.prepareMessage('camera.getPicture');

				cordova.cache[message.uuid] = function(message) {
					if (message.data.success) {
						success(message.data.success);
					} else if (message.data.error) {
						error(message.data.error);
					}

					delete cordova.cache[message.uuid];
				};

				message.params.options = options || {};
				cordova.sendMessage(message);
		};

	if (cordova) {
		if (isUndefined(navigator.camera) || isUndefined(navigator.camera.getPicture)) {
			navigator.camera = navigator.camera || {};
			navigator.camera.getPicture = getPicture;

			window.Camera = {
				DestinationType: {
					DATA_URL : 0,      // Return image as base64-encoded string
					FILE_URI : 1,      // Return image file URI
					NATIVE_URI : 2     // Return image native URI (e.g., assets-library:// on iOS or content:// on Android)
				},
				PictureSourceType: {
					PHOTOLIBRARY : 0,
					CAMERA : 1,
					SAVEDPHOTOALBUM : 2
				},
				EncodingType: {
					JPEG : 0,          // Return JPEG encoded image
					PNG : 1            // Return PNG encoded image
				},
				MediaType: {
					PICTURE: 0,        // allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType
					VIDEO: 1,          // allow selection of video only, WILL ALWAYS RETURN FILE_URI
					ALLMEDIA : 2       // allow selection from all media types
				},
				Direction: {
					BACK : 0,          // Use the back-facing camera
					FRONT : 1          // Use the front-facing camera
				}
			};
		}
		
	}
})(window, window.cordova);