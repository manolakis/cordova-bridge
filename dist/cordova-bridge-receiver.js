/*! cordova-bridge - v0.1.0 - 2013-11-01
* Copyright (c) 2013 ; Licensed  */
(function(window){
	'use strict';

	var document = window.document, 
		_domain,
		iframe,
		plugins = {},

		helper = {
			trigger: function(obj, name) {
				var e = document.createEvent('Event');
				e.initEvent(name, true, true);
				obj.dispatchEvent(e);
			}
		},

		receiveMessage = function(event) {
			if (event.origin === _domain) {
				var data = event.data;
				if (data.signature in plugins){
					plugins[data.signature].apply({uuid: data.uuid, params: data.params});
				}
			}
		},

		loadExternalApplication = function() {
			iframe = document.createElement('iframe');
			iframe.setAttribute('src', this.url);
			document.body.appendChild(iframe);
			helper.trigger(document, 'applicationready');
		},

		onDeviceReady = function() {
			window.addEventListener('message', receiveMessage, false);
			loadExternalApplication.call(window.app);
		},

		bindEvents = function() {
			document.addEventListener('deviceready', onDeviceReady, false);
		},

		postMessage = function(data){
			iframe.contentWindow.postMessage(data, _domain);
		};

	window.app = {
		initialize: function(domain, url) {
			_domain = domain; //domain.lastIndexOf('/') === domain.length -1 ? domain + url : domain + '/' + url;
			this.url = domain + url; //domain.lastIndexOf('/') === domain.length -1 ? domain + url : domain + '/' + url;
			bindEvents();
		},

		addPlugin: function(plugin) {
			Object.keys(plugin).forEach(function(key){
				plugins[key] = plugin[key];
			});
		},

		response: function(data) {
			postMessage({
				uuid: this.uuid,
				data: data
			});
		}
	};

})(this);
(function(window){

	window.app.addPlugin({
		'camera.getPicture': function(){
			var uuid = this.uuid,
				options = this.params.options || {};

			navigator.camera.getPicture(function(imageData){
				window.app.response.apply({ uuid: uuid }, [{ success: imageData }]);
			}, function(message) {
				window.app.response.apply({ uuid: uuid }, [{ error: message }]);
			}, options);
		},

		'camera.cleanup': function(){
			var uuid = this.uuid;

			navigator.camera.cleanup(function(){
				window.app.response.apply({ uuid: uuid }, [{ success: true }]);
			}, function(message){
				window.app.response.apply({ uuid: uuid }, [{ error: message }]);
			});
		}
	});

})(this);
(function(window){

	window.app.addPlugin({
		'device': function(){
			window.app.response.apply(this, [{ device: window.device}]);
		}
	});

})(this);