/*! cordova-bridge - v0.1.0 - 2013-11-01
* Copyright (c) 2013 ; Licensed  */
(function(window){
	'use strict';

	var s4 = function() {
			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
		},
		
		helper = {
			domain: 'http://localhost:3000',
			uuid : function() {
				return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
			},
			trigger: function(obj, name) {
				var e = window.document.createEvent('Event');
				e.initEvent(name, false, false);
				obj.dispatchEvent(e);
			},
			sendMessage: function(data) {
				window.parent.postMessage(data, 'file://');
			},
			prepareMessage: function(signature) {
				return {
					uuid: helper.uuid(),
					signature: signature,
					params: {}
				};
			}
		},
		cache = {};

	if (window.self !== window.top) {
		window.cordova = {
			cache: cache,
			sendMessage: helper.sendMessage,
			prepareMessage: helper.prepareMessage
		};

		window.addEventListener('message', function(event){
			if (event.origin === 'file://') {
				var data = event.data;
				if (data.uuid in cache) {
					cache[data.uuid](data);
				}
			}
		}, false);
	}
	
})(this);
(function(window, cordova){
	'use strict';

	var isUndefined = function(value) {
			return typeof value === 'undefined';
		},

		getPicture = function(success, error, options){
			var message = cordova.prepareMessage('camera.getPicture');

				cordova.cache[message.uuid] = function(message) {
					if (message.data.success) {
						success(message.data.success);
					} else if (message.data.error) {
						error(message.data.error);
					}

					delete cordova.cache[message.uuid];
				};

				message.params.options = options || {};
				cordova.sendMessage(message);
		};

	if (cordova) {
		if (isUndefined(navigator.camera) || isUndefined(navigator.camera.getPicture)) {
			navigator.camera = navigator.camera || {};
			navigator.camera.getPicture = getPicture;

			window.Camera = {
				DestinationType: {
					DATA_URL : 0,      // Return image as base64-encoded string
					FILE_URI : 1,      // Return image file URI
					NATIVE_URI : 2     // Return image native URI (e.g., assets-library:// on iOS or content:// on Android)
				},
				PictureSourceType: {
					PHOTOLIBRARY : 0,
					CAMERA : 1,
					SAVEDPHOTOALBUM : 2
				},
				EncodingType: {
					JPEG : 0,          // Return JPEG encoded image
					PNG : 1            // Return PNG encoded image
				},
				MediaType: {
					PICTURE: 0,        // allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType
					VIDEO: 1,          // allow selection of video only, WILL ALWAYS RETURN FILE_URI
					ALLMEDIA : 2       // allow selection from all media types
				},
				Direction: {
					BACK : 0,          // Use the back-facing camera
					FRONT : 1          // Use the front-facing camera
				}
			};
		}
		
	}
})(window, window.cordova);
(function(window, cordova){
	'use strict';

	if (cordova) {
		var getDeviceModel = function(){				
				var message = cordova.prepareMessage('device');

				cordova.cache[message.uuid] = function(message) {
					window.device = message.data.device;

					delete cordova.cache[message.uuid];
				};

				cordova.sendMessage(message);
			};
		
		getDeviceModel();
		
	}
})(window, window.cordova);