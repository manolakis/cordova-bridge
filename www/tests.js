(function($){
	'use strict';

	var content,

		clearScreen = function() {
			content.empty();
		},

		newRow = function() {
			return $('<div class="row"><div class="col-md-12"></div>');
		},

		addTitle = function(title) {
			content.append('<h3>' + title + '</h3>');
		},

		testDevice = function(){
			var addRow = function(title, data) {
				var row = newRow().find('.col-md-12').append('<strong>'+ title+ '</strong>: ' + data);
				content.append(row);
			};

			addTitle('Device');
			addRow('model', window.device.model);
			addRow('cordova', window.device.cordova);
			addRow('platform', window.device.platform);
			addRow('uuid', window.device.uuid);
			addRow('version', window.device.version);
		},

		testCamera = function(){
			var testGetPicture = function(text, options){
				var cameraPicture = $('<div />'),
					getPictureBtn = $('<button class="btn btn-primary btn-block">' + text + '</button>');

				getPictureBtn.bind('click', function(){

					navigator.camera.getPicture(function(image){
						cameraPicture.empty();
						var row = newRow().find('.col-md-12')
									.append('<img src="data:image/jpeg;base64,' + image + '" class="img-rounded img-responsive">');
						cameraPicture.append(row);						
					}, function(message){
						cameraPicture.empty();
						var row = newRow().find('.col-md-12')
									.append('<strong>Error</strong>: ' + message);
						cameraPicture.append(row);
					}, options);
				});

				cameraPicture.append(getPictureBtn);
				content.append(cameraPicture);	
			};

			addTitle('Camera');
			testGetPicture('Hacer foto', {
				quality: 50,
				destinationType: Camera.DestinationType.DATA_URL
			});
			testGetPicture('Seleccionar foto de la galería', {
				quality: 50,
				destinationType: Camera.DestinationType.DATA_URL,
				sourceType: Camera.PictureSourceType.PHOTOLIBRARY
			});
		};

	$(document).ready(function(){
		content = $('#content');

		$('#startBtn').bind('click', function(){
			clearScreen();
			testDevice();
			testCamera();
		});
	});
})(jQuery);