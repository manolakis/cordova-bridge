var express = require('express')
	, connect = require('connect')
    , http = require('http')
    , path = require('path')
    , app = express();

console.log('loading app');

app.configure(function(){
	app.use(connect.compress());
    app.use('/', express.static(path.join(__dirname, 'www')));
    app.use('/js', express.static(path.join(__dirname, 'lib')));

/*
    app.get("/cache.manifest", function(req, res){
    	res.header("Content-Type", "text/cache-manifest");
    	res.sendfile('cache.manifest', {root: 'target/public'});
    });
*/
});

var server = http.createServer(app).listen(3000, function(){
    console.log("Express server listening on port 3000");
});